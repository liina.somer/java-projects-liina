package ee.bcs.valiit.day7;

public class FerrariF355 implements Movable {
    @Override
    public String getManufacturer() {
        return "Ferrari";
    }

    @Override
    public double getMaxSpeed () {
        return 320;
    }

    @Override
    public int getProductionYear() {
       return 1993;
    }

    @Override
    public void speedUp(double finalSpeed) {
        System.out.println("The speed is increasing quickly..");
    }

    @Override
    public void slowDown(double finalSpeed) {
        System.out.println("Breaks are good, slowing down..");

    }
}
