package ee.bcs.valiit.day2;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Day2Excersises {
    public static void main(String[] args) {
        // Defineeri muutuja, mis hoiaks väärtust 456.78.
        double myNumber = 456.78;
        //Defineeri muutuja, mis viitaks tähtede jadale "test".
        String listOfLetters = "test";
        boolean theAbsoluteTruth = !false;
        //Defineeri kaks erinevat tüüpi muutujat, mis hoiaksid endas väärtust 'a'.
        char myCharacter = 'a';
        String myCharacter1 = "a";
        //Defineeri muutuja, mis viitaks numbrite kogumile 5, 91, 304, 405.
        int[] listOfNumbers = {5, 91, 304, 405};
        //Defineeri muutuja, mis viitaks numbrite kogumile 56.7, 45.8, 91.2.
        double[] listOfNumbers2 = {56.7, 45.8, 91.2};
        //Defineeri muutuja, mis viitaks järgmiste väärtuste kogumile:
        //"see on esimene väärtus"
        //67
        //58.92
        Object [] myStuff = {"see on esimene väärtus", 67, 58.92};
        //Defineeri muutuja, mis viitaks väärtusele 7676868683452352345324534534523453245234523452345234523452345 ja mis ei oleks String-tüüpi.
        BigInteger myVeryLargeNumber = new BigInteger("7676868683452352345324534534523453245234523452345234523452345");
        BigDecimal myVeryLargeDecimal = new BigDecimal("7676868683452352345324534534523453245234523452345234523452345.357654");

        System.out.println(myStuff);

    }

}
