package ee.bcs.valiit.day9;

import ee.bcs.valiit.bank.Account;
import ee.bcs.valiit.bank.AccountService;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SortingExcersises {
    public static void main(String[] args) throws IOException {
        AccountService.loadAccounts("resources/kontod.txt");
        List<Account> customers = AccountService.getAccounts();

        System.out.println("Sorteerime konto jäägi järgi kahanevalt...");
        //variant 1, collections klassi sort, sordi collections ära (2 parameetrit), aga identne customers.sort meetodiga
        //Collections.sort(customers, (a1, a2) -> (int) (a2.getBalance() - a1.getBalance()));

        //variant 2, sorteeri ennast ära(1 parameeter)
      customers.sort((a1, a2) -> (int) (a2.getBalance() - a1.getBalance()));
        System.out.println(customers);

        System.out.println("Sorteerime ees- ja perenime järgi...");
        Collections.sort(customers, (a1, a2) -> {
            if (a1.getFirstName().compareTo(a2.getFirstName()) != 0) {
                return a1.getFirstName().compareTo(a2.getFirstName());
            } else {
                return a1.getLastName().compareTo(a2.getLastName());
            }


        });
        System.out.println(customers);
        System.out.println("Otsime (filtreerime) välja rikkad kliendid, rikastel on üle 1000 euro");
        //teeme rikaste listi
        List<Account> richCustomers = customers.stream().filter( a -> a.getBalance() > 1000 ).collect(Collectors.toList());
        //sorteerime ära kasvavalt
        richCustomers.sort((a1, a2) -> (int) (a1.getBalance() - a2.getBalance()));
        System.out.println(richCustomers);
    }
}
