package ee.bcs.valiit.day7;

public class EstonianDog extends Dog {
    public EstonianDog(String name) {
        super(name);
    }

    @Override
    public void bark() {
        System.out.println("auh-auh");//Estonian → auh-auh; auch-auch
    }

}
