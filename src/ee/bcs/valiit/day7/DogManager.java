package ee.bcs.valiit.day7;

public class DogManager {
    public static void main(String[] args) {
        System.out.println("Belgian dog");
        Dog belgianDog = new BelgianDog("Apollo");
        System.out.println(belgianDog);
        belgianDog.bark();

        System.out.println("English dog");
        Dog englishDog = new EnglishDog("Bella");
        System.out.println(englishDog);
        englishDog.bark();

        System.out.println("Estonian dog");
        Dog estonianDog = new EstonianDog("Muki");
        System.out.println(estonianDog);
        estonianDog.bark();


        }
    }

