package ee.bcs.valiit.day6;

public class MethodName {
    //kui kaugele peab funktsioon nähtav olema
    //private kui ainult siin klassis nähtav
    //static
    //int tagastab int tüüpi
    //meetodi algus väike täht ja käskivas kõneviisis mingi tegevus

    public static void main(String[] args) {
        //Funktsiooni välja kutsumine ehk käivitamine

        int myCalculatedResult = doSomePointlessCalculation(1, 2, 3);
        System.out.println(myCalculatedResult);
        //vaja main meetodis välja kutsuda
        //avaldises seda kasutada ei saa, kuna see oli void, ei tagasta midagi
        printGreetingToConsole();
        printAdvancedGreeting("Marek");
    }

    //sulgudes on sisendparameetrid
    //Funktsiooni defineerimine
    private static int doSomePointlessCalculation (int number1, int number2, int number3) {
        int result = (number1 + number2 + number3)/ 2;
        //return ´null´ ei saa, kuna primitiivse muutuja puhul ei saa väärtus null olla
        return result;
    }

    private static void printGreetingToConsole () {
        //siit ei prindi seda
        System.out.println("day1.HelloWorld!");

    }

    private static void printAdvancedGreeting (String name) {

        if (name.equals("Matthew")) {
            return;
        }
            System.out.println("Hello " + name + "!");
        }



}
