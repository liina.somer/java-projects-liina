package ee.bcs.valiit.day6;

public class OOPBasics {

    public static void main(String[] args) {

        //uus pet1, mille tüübiks ee.bcs.valiit.day6.Pet, new puhul läheb sinna ee.bcs.valiit.day6.Pet klassi ja vaatab
        //mis pole static, siis teeb sellest koopia
        Pet pet1 = new Pet();
        pet1.name = "Muki";
        pet1.introduce();
      //  ee.bcs.valiit.day6.Pet.introduce(pet1);

        Pet pet2 = new Pet();//on kaks isendit, mis on koopiad klassist
        pet2.name = "Rex";


        System.out.println("Ex 3");

        System.out.println("Hakkan looma objekti ee.bcs.valiit.day6.Person klassist...");
        Person person1 = new Person("49403136526");
        System.out.println("Objekt loodud!");
   //     person1.personalCode = "49403136526"; //seda ei saa kui on konstruktor ee.bcs.valiit.day6.Person all this. ..
        System.out.println(person1.getGender());
        System.out.println(person1.getBirthYear());
        System.out.println(person1.getBirthMonth());
        System.out.println(person1.getBirthDayOfMonth());



    }
}
