package ee.bcs.valiit.day8;

public class NatureParkGuests {
    private String date;
    private int count;

    //counter, splitib koma juurest ära, date on esimene element ja count teine
    public NatureParkGuests(String visitString) {
        String[] props = visitString.split(", ");
        this.date = props[0];
        this.count = Integer.parseInt(props[1]);
    }


//getterid ja toString
    public String getDate() {
        return date;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "NatureParkGuests{" +
                "date='" + date + '\'' +
                ", count=" + count +
                '}';
    }


}

