package ee.bcs.valiit.day7;

public class RenaultMegane implements Movable {
    @Override
    public String getManufacturer() {
        return "Renault";
    }

    @Override
    public double getMaxSpeed() {
        return 170;
    }

    @Override
    public int getProductionYear() {
        return 2008;
    }

    @Override
    public void speedUp(double finalSpeed) {
        System.out.println("The speed is increasing slowly...");
    }

    @Override
    public void slowDown(double finalSpeed) {
        System.out.println("The speed is slow already...");
    }
}
