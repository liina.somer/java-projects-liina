package ee.bcs.valiit.day2;

public class Branching {
    public static void main(String[] args) {
        boolean isValid = true;

        //tingimustehe ehk inline-if
        int i = 9;
        //inline-if, ei kasutata kui keerulisem
        String canDivideByThree = i % 3 == 0 ? "jagub kolmega" : "ei jagu kolmega";
        System.out.println(canDivideByThree);

        //võib seda varianti kasutada pigem
        if (i % 3 == 0) {
            canDivideByThree = "jagub kolmega";
        } else {
            canDivideByThree = "ei jagu kolmega";
        }
        System.out.println(canDivideByThree);

        //switch statement
        //kasulik kasutada kui
        //a)lahenduvate väärtuste hulk ei ole lõpmatu
        //b)kui hargnemisi on palju, rohkem kui 3-4
        //klassikaline switch:
        String trafficLightColor = "green";
        switch (trafficLightColor) {
            case "red":
                System.out.println("Cars must stop and wait.");
                //another statements...
                break;
            case "yellow":
                System.out.println("Cars must slow down.");
                break;
            case "green":
                System.out.println("Cars can drive.");
                //break;
            default:
                System.out.println("Follow the speed limit!");
        }
    }
}
