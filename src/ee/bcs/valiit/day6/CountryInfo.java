package ee.bcs.valiit.day6;

import java.util.Arrays;

public class CountryInfo {
public String countryName;
public String capital;
public String primeMinister;
public String[] languages;



    public CountryInfo(String countryName, String capital, String primeMinister, String[] languages) {
        this.countryName = countryName;
        this.capital = capital;
        this.primeMinister = primeMinister;
        this.languages = languages;


    }

    @Override
    public String toString() {
        return "day5.CountryInfo{" +
                "countryName='" + countryName + '\'' +
                ", capital='" + capital + '\'' +
                ", primeMinister='" + primeMinister + '\'' +
                ", languages=" + Arrays.toString(languages) +
                '}';
    }

}

