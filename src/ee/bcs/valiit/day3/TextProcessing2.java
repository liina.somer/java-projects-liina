package ee.bcs.valiit.day3;

import java.util.Scanner;

public class TextProcessing2 {

    //screaming snake case tava konstandi puhul: 1.suured tähed ja alakriipsud
    public static final double VALU_ADDED_TAX_RATE = 1.2;

    public static void main(String[] args) {
        //muutumatu muutuja
        final String a = "Hello!";
        //final-it ei saa enam muuta
        //teen muutujaga a asju
        //a = "Good bye!";

 //kui on klassi tasemel final muutuja ehk konstant,
        //siis ei saa muuta
        // valuAddedTaxRate = 1.3;

        String myStr1 = "aaa";
        String myStr2 = "bbb";
        myStr1 = myStr1 + myStr2;//tekitatakse täiesti uus stringi objekt
        //objekti muuta ei saa
        //sellised operatsioonid tekitavad jõudlusprobleeme
        //kui manipuleerime väga suuri stringe
        //lahendus StringBuilder või StringBuffer-
        // neil sisuliselt vahet pole, jõudluse parandajad
        StringBuilder sb = new StringBuilder();//tekitan uue objekti
        sb.append("aaa");//nendega uus
        sb.append("bbb");
        System.out.println(sb.toString());

        String names = "Mari, Malle, Mart, Meelis";
        Scanner myScanner = new Scanner(names);
       // myScanner.useDelimiter();
        System.out.println(myScanner.next());

    }
}
