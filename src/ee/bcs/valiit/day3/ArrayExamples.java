package ee.bcs.valiit.day3;

public class ArrayExamples {
    public static void main(String[] args) {
        // int a = 5;
        // int[] myArray; //intide massiiv, kui väärtust ei anna
        //siis automaatselt väärtus null ehk puudub
        // myArray = new int[5]; //loo uus objekt
        //5 kohta tekib, väärtuseks by default {0, 0, 0, 0, 0}
        //Integer[] myNonPrimitiveArray = new Integer[5];
        //mitteprimitiivsel pole väärtust {null, null, null, null, null}
        // myArray[2] = 5;//mitmenda elemendi väärtus, alustab nullist
        //ehk siis see on kolmas element
        //myArray.length;

       /* String[] args = new String[3];
        args[0] = "Milano";
        args[2] = "4";
        System.out.println(args[0]);

        String[] suits = {"Ruutu", "Risti", "Ärtu", "Poti"};
        suits = String[4];

        int[] u = {}
        u = int[0] {}*/ //0 elementi pikk massiiv võib anda ka infot
        //võib kasutada näiteks rahvastikuregistris, et näidata, et pole lapsi
        //või et registris pole liiklusõnnetusi


        //kahedimensiooniline. võib ka rohkem dimensioone olla
        //aga lihtsam teha siis erinevate klassidega juba
        //inline meetodil
        int[][] monthlyTemperatures = {
                {-7, 3},
                {-9, 7},
                {5, 12},
                {13, 15},
                {7, 18},//juuni
                {16, 23},
                {16, 17},
                {8, 11},
                {5, 8},
                {3, 7},//november
                {14, 1},


        };
        //klassikaline viis
        //
        int[][] monthlyTemperatures2 = new int[12][2];

        //jaanuar
        monthlyTemperatures2 [0][0] = -7;
        monthlyTemperatures2 [0][1] = 3;
        //veebruar
        monthlyTemperatures2 [1][0] = -9;
        monthlyTemperatures2 [1][1] = 7;
        //märts
        monthlyTemperatures2 [2][0] = 0;
        monthlyTemperatures2 [2][1] = 5;


        System.out.println("Juunikuu maksimaalne temperatuur: " + monthlyTemperatures[5][1]);
        System.out.println("Novembrikuu minimaalne temperatuur: " + monthlyTemperatures[10][0]);


    }
}
