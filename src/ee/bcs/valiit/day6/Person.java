package ee.bcs.valiit.day6;

public class Person {
    public String personalCode;

    public Person(String personalCode){
        System.out.println("Konstruktor käivitus..");
        this.personalCode = personalCode;//this vajalik, lokaalse muutuja väärtus objekti sisse
    }


    public String getGender() {
        int firstDigit = Integer.parseInt(this.personalCode.substring(0, 1));
        return firstDigit % 2 == 1 ? "M" : "F";

    }

    public int getBirthYear() { //siin ei ole sulgudes vaja String personalCode, saab ise aru
        //valideerimine
        if (this.personalCode == null) {
            return -1;

        } else if (this.personalCode.length() != 11) {
            return -1;
        }

        int century = Integer.parseInt(personalCode.substring(0, 1));// nr 4
        int birthYear = Integer.parseInt(personalCode.substring(1, 3));// nr 94

        switch (century) {
            case 1:
            case 2:
                return 1800 + birthYear;//break pole vaja, kuna return nagunii jätab pooleli
            case 3:
            case 4:
                return 1900 + birthYear;
            case 5:
            case 6:
                return 2000 + birthYear;
            case 7:
            case 8:
                return 2100 + birthYear;
            default:
                return -1;

        }
    }
    public String getBirthMonth (){
       int birthMonth = Integer.parseInt(this.personalCode.substring(3, 5));
        switch (birthMonth) {
            case 1:
                return "January;";
            case 2:
                return "February;";
            case 3:
                return "March;";
            case 4:
                return "April;";
            case 5:
                return "May;";
            case 6:
                return "June;";
            case 7:
                return "July;";
            case 8:
                return "August;";
            case 9:
                return "September;";
            case 10:
                return "October;";
            case 11:
                return "November;";
            case 12:
                return "December;";
            default:
                return "N/A";
        }

    }
    public int getBirthDayOfMonth() {
        int BirthDayOfMonth = Integer.parseInt(this.personalCode.substring(5, 7));
        return BirthDayOfMonth;

    }


}
