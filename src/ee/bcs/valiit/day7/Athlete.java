package ee.bcs.valiit.day7;

//iga klass by default pärineb Objectist, võiks kirjutada extend Object aga ei kirjutata
public abstract class Athlete extends Human {
    private String firstName;
    private String lastName;
    private int age;
    private String gender;
    private double height;
    private double weight;


    public Athlete(String firstName, String lastName, int age, String gender, double height, double weight) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.gender = gender;
        this.height = height;
        this.weight = weight;
    }

    public abstract void perform();





}
