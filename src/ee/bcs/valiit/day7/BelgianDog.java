package ee.bcs.valiit.day7;

public class BelgianDog extends Dog {
    public BelgianDog(String name) {
        super(name);
    }

    @Override
    public void bark() {
        System.out.println("wooah-wooah");
    }
    //wooah-wooah
}
