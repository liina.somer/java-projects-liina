package Pranglija;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PranglijaApp {
    private static final int GUESS_COUNT = 2;
    private static List<GameResult> gameLog = new ArrayList<>();
    //private static final int MAX_NUMBER = 5;
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("------------------");
        System.out.println("Pranglija äpp");
        System.out.println("------------------");


        while(true) {
            System.out.print("Sisesta oma nimi: ");
            String playersName = scanner.nextLine();

            System.out.print("Sisesta genereeritavate arvude miinimumväärtus: ");
            int rangeMin = getIntFromConsole();
            //(int)(Math.random() * (rangeMin + 1)) + rangeMin:

            System.out.print("Sisesta genereeritavate arvude maksimumväärtus: ");
            int rangeMax = getIntFromConsole();
            //(int)(Math.random() * (rangeMax + 1)) + rangeMax:

            //Mäng
            //01.01.1970 00:00:00- ajaarvamise algus
            long startTime = System.currentTimeMillis();
            //System.out.println(startTime);

            //Must maagia
            //10 korda saab arvata
            int count = 0;
            for (; count < GUESS_COUNT; count++) {
                //System.out.println("You have " + i + " guess(es) left. Choose again:");
                int randomNumber1 = (int) (Math.random() * (rangeMax -rangeMin + 1)) + rangeMin;
                int randomNumber2 = (int) (Math.random() * (rangeMax -rangeMin + 1)) + rangeMin;
                //boolean hasWon = false;
                System.out.print(String.format("%d + %d = ", randomNumber1, randomNumber2));
                int guess = Integer.parseInt(scanner.nextLine());//parem n kasutada nextline´i
                if (randomNumber1 + randomNumber2 != guess) {

                    break;
                    //System.out.println("Sa kaotasid!");
                } //else {
                // System.out.println
            }

            if (count == GUESS_COUNT) {
                System.out.println("Tubli!");

                long endTime = System.currentTimeMillis();
                double elapsedTime = endTime - startTime;
                elapsedTime = elapsedTime/ 1000.0;
                System.out.println("Mäng kestis " + elapsedTime + " sekundit.");

                //logime mängu gameLog muutujasse
                //private static List<String> gameLog = new ArrayList<>();
//                gameLog.add(playersName);
//                gameLog.add(String.valueOf(elapsedTime));
//                gameLog.add(String.valueOf(rangeMin - rangeMax));
                //gameLog.add(String.valueOf(rangeMax));
                //tekitame objekti mängu tulemuse hoidmiseks (instruktsioon GameResult)
               //muutuja mille tüübiks on klass GameResult
                GameResult result = new GameResult();
                result.name = playersName;
                result.time = elapsedTime;
                result.numberRange = rangeMin + "-" + rangeMax;
                gameLog.add(result);
                gameLog.sort((resultA, resultB) -> { //lambda avaldis??
                    if (resultA.time > resultB.time) {
                        return 1;//vaheta järjekord ära, selle kohta dokumentatsioon, kokku lepitud
                        //pos number tähendab, et muuda jrk ja neg ei muuda
                    } else {
                        return -1;//järjekord ongi juba õige, ära muuda järjekorda
                    }
                });
               // gameLog.add(String.format("%s, %f sekundit, %d - %d", playersName, elapsedTime, rangeMin, rangeMax));

                //Kuvame tulemuse
                System.out.println("Mängu logi...");
                for (GameResult gameResult : gameLog) {
                    System.out.println("------------------");
                    System.out.println("Nimi:\t\t\t" + gameResult.name);
                    System.out.println("Aeg:\t\t\t" + gameResult.time + " sekundit");
                    System.out.println("Numbrivahemik:\t" + gameResult.numberRange);

                }


            } else {
                System.out.println("Sa kaotasid");
            }


            System.out.println("Mäng on lõppenud. Mida soovid edasi teha?\n" +
                    "\t0 - välju mängust\n" +
                    "\t1 - alusta uut mängu\n");
            int userCommand = getIntFromConsole();
            if (userCommand == 0) {
                break;
            }

        }


        System.out.println("------------------");
        System.out.println("Copyright: Liina Sõmer (all rights reserved)");
        System.out.println("------------------");
    }

    private static int getIntFromConsole() {
      while(true)  {
          try {
        return Integer.parseInt(scanner.nextLine());
    } catch (NumberFormatException e) {
              System.out.println("Sisestatud tekst ei ole number. Proovi uuesti!");
              System.out.print("Sisesta number: ");
          }
          }


    }
}
