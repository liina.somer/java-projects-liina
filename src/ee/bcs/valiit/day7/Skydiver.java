package ee.bcs.valiit.day7;

public class Skydiver extends Athlete {
    //public double maxSpeed;
    //kui on ainult 1 Skydiver, siis võib siin andmed ette anda
    public Skydiver(String firstName, String lastName, int age, String gender, double height, double weight) {
        super(firstName, lastName, age, gender, height, weight);

    }

    @Override
    public void perform(){
        System.out.println("Falling from sky");
    }
//    @Override
//    public String toString() {
//        return "Skydiver{}";
    }



