package ee.bcs.valiit.day7;

public class AthleteManager {
    public static void main(String[] args) {

//        Athlete athlete1 = new Athlete();//vaatleme kui atleeti
//        Object athlete2 = new Athlete();//vaatleme kui objekti, ei saa teha, kui Athlete on abstract class

//Athlete skydiver = new Skydiver();
//Athlete runner = new Runner();

        Runner runner1 = new Runner("Jack", "Jones", 32, "M", 1.78, 85.5);
        Runner runner2 = new Runner("Jesse", "Jones", 35, "M", 1.85, 80.0);
        Skydiver skydiver1 = new Skydiver("Josephine", "Jones", 28, "F", 1.70, 65.5);
        Skydiver skydiver2 = new Skydiver("Kelly", "Jones", 30, "F", 1.60, 55.5);



runner1.perform();
runner2.perform();
skydiver1.perform();
skydiver2.perform();


//Human h1 = new Human();
//h1.setFirstName("M");
//System.out.println(h1.getFirstName());

    }
}
