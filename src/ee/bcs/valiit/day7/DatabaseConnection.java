package ee.bcs.valiit.day7;

import javax.xml.crypto.Data;

public class DatabaseConnection {
    private static DatabaseConnection connection = null;
    private DatabaseConnection() {

    }
    public static DatabaseConnection getConnection(){
        if (connection==null){
            connection = new DatabaseConnection();
        }
        return connection;
    }
}
