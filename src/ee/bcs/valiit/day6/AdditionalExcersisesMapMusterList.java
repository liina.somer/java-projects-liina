package ee.bcs.valiit.day6;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdditionalExcersisesMapMusterList {
    public static void main(String[] args) {
        System.out.println("Ex 1");


        //kahe for tsükliga
        //8 rida ja 19 tulpa
        for (int row = 1; row <= 8; row++) {
            for (int col = 1; col <= 19; col++) {
                //inline if kui column paaritu, siis #
                // System.out.print((col + row) % 2 == 0 ? "#" : "+");
                if (row % 2 == 1) {
                    System.out.print(col % 2 == 1 ? "#" : "+");
                } else {
                    System.out.print(col % 2 == 1 ? "+" : "#");
                }
            }
            System.out.println();
        }

        System.out.println("Ex 1 muster 2");
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j < i; j++) {
                System.out.print("-");
            }
            System.out.println("#");
        }

        System.out.println("Ex 1 muster 3");
        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= 7; j++) {
                if (i % 2 == 0) {
                    System.out.print("=");
                } else {
                    System.out.print(j % 3 == 1 ? "#" : "+");
                }
            }
            System.out.println();
        }

        System.out.println("Ex 2");

        //Stringina
        String personsString = "Eesnimi: Teet, Perenimi: Kask, Vanus: 34, Amet: lendur, Kodakondsus: Eesti; Eesnimi: Mari, Perenimi: Tamm, Vanus: 56, Amet: kosmonaut, Kodakondsus: Soome; Eesnimi: Kalle, Perenimi: Kuul, Vanus: 38, Amet: arhitekt, Kodakondsus: Läti; Eesnimi: James, Perenimi: Cameron, Vanus: 56, Amet: riigiametnik, Kodakondsus: UK; Eesnimi: Donald, Perenimi: Trump, Vanus: 73, Amet: kinnisvaraarendaja, Kodakondsus: USA";
        //splitin suure stringi personiteks laiali, inimesed eraldi massiivi elementides
        //on 5 elemendiline massiiv
//        String [] personsArray = personsString.split(";");
//
//        //lõpp eesmärk 2 tasandiline massiiv, ma tean, et iga inimene saab kirje esimesele tasemele ja teisel tasemel 5 kuna andmeid on nii palju
//        String[][] personDataArray = new String[personsArray.length][5];
//        //ükshaaval käin läbi kõik personids
//        for (int i = 0; i <= personsArray.length; i++) {
//            //person kohal i vahel on komad, splitib selle ära
//            String [] personArray = personsArray[i].split(",");
//            for (int j = 0; j < personArray.length; j++) {
//                //: kohapealt splitin ära, eesnimi: kaob ära, jääb ainult Teet ehk [1]
//                //käin 5 korda läbi selle, et jääk ainult andmed
//                //eesmärgiks puhas data- vaadata Evaluate all, kuidas lisab ja eemaldab midagi
//                personArray[j] = personArray[j].split(": ")[1];
//            }
//            personDataArray[i] = personArray;
//        }
//        //personi kaupa array
//        for (String[] person: personDataArray) {
//            System.out.println("---------------");
//            System.out.printf("%s / %s / %s aastat /ametilt %s / kodakondsus %s \n", person[0], person[1], person[2], person[3], person[4]);
//            System.out.println("---------------");
//
//        }
//lõpptulemus String [][] {"Teet", "Kask", "34", "lendur", "Eesti"}

//        System.out.println("List demo");
//        //List variant
//        List<List<String>> personDataList = new ArrayList<>();
//        for (String personLine : personsString.split("; ")) {
//            List<String> personData = new ArrayList<>();
//            for (String personProperty : personLine.split(", ")) {
//                personDataList.add(personData);
//            }
//            //           System.out.println(personDataList);
//
//
//        }
//        for (List<String> person : personDataList) {
//            System.out.println("---------------");
//            System.out.printf("%s / %s / %s aastat /ametilt %s / kodakondsus %s \n", person.get(0), person.get(1), person.get(2), person.get(3), person.get(4));
//            System.out.println("---------------");
//
//
//        }
//
//        System.out.println("Map demo");
//        Map<String>, List<String>> personDataMap = new HashMap<>();
//        for (String personLine : personsString.split("; ")) {
//            String[] personPropArray = personLine.split(", ");
//            String key = personPropArray[0] + " " + personPropArray[1];
//            List<String> personData = new ArrayList<>();
//            for (int i = 2; i < personPropArray.length; i++) {
//                personData.add (personPropArray[i]);
//
//            }
//            personDataMap.put(key, personData);
//        }
//        for (String personFullName : personDataMap.keySet()) {
//            List<>
//        }


    }
}






