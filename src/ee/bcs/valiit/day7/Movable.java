package ee.bcs.valiit.day7;

public interface Movable {
    /*Defineeri liides (interface) Movable ja lisa sellele mõned liikumisvahendile iseloomulikud meetodid (näiteks speedUp(), slowDown(), …)
Tekita klass Car ja lisa sellele mõned omadused ja käitumised. Tuleta seejärel veel kolm klassi, mis pärinevad Car klassist:
Ferrari F355
Ford Fiesta
Renault Megane
Implementeeri Movable liides Car klassis.


Defineeri mõned Movable tüüpi muutujad ja kasuta neid autoobjektide hoidmiseks ning auto-objektidega manipuleerimiseks.
*/
    String getManufacturer();
    double getMaxSpeed();
    int getProductionYear();
    void speedUp(double finalSpeed);
    void slowDown(double finalSpeed);


}
