//public class LoopDemo {
//    public static void main(String[] args) {
////i++ tuleb panna, muidu ei lõppegi tsükkel
//        //muidu jääbki ketrama
////        System.out.println("Tsükli algus...");
////       // int i = 0;
////       // for (;i < 6; i++){
////        for (int i = 0; i < 5; i++)
////            System.out.println("Tsükli kord: " +i);
////        }
//        //kui i defineerida väljapool tsüklit, siis saab nii ka:
//        // System.out.println("Tsükli lõpp: " + i);
//        //System.out.println("Tsükli lõpp: ");
//
//        String[] suits = {"Ruutu", "Risti", "Ärtu", "Poti"};
////        for (int t = 0, t < suits.length -1; t++){
////        System.out.println("Mast: " +suits[t]);
////        }
//
//        for (String suit : suits) {
//            System.out.println("Mast (foreach meetodil): " + suit);
//        }
//
//        //while tsükkel
////        while(true) {
////
////            //palun sisesta number
////            //kui kasutaja sisestab tähe, siis otsast peale
////            //kui paneb korrektse numbri, siis lõppeb
////            //kuidas lõpetada sükkel?
////            if(korrektneArv){
////                break;
////            }
//    }
//    // on igavene tsükkel
//    //võib vahel vaja olla, et kestaks igavesti, nt microcontroller kontrollib
//    //kella kogu aeg
//
//    int suitIndex = 0;
//        while(suitIndex<suits.length)
//
//    {
//        System.out.println("Mast (while tsükliga): " + suits[suitIndex]);
//        suitIndex++;
//    }
//
//    //do..while tsükkel
//    //enne tee ja siis mõtle
//    int suitIndex2 = 0;
//    do
//
//    {
//        System.out.println("Mast (do..while tsükliga): " + suits[suitIndex]);
//        suitIndex2++;
//
//    } while(suitIndex2<suits.length);
//}
//}
