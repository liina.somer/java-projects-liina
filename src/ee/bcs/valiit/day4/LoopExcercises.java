package ee.bcs.valiit.day4;

public class LoopExcercises {
    public static void main(String[] args) {


        //
        System.out.println("Ex 11");
        int number = 1;
        while (number <= 100) {
            System.out.println(number);
            number += 1;
        }

//        System.out.println("Ex 12");
//        for (i = 1;i < 101; i++) {
//            System.out.println(i);
//        }
//
//        System.out.println("Ex 13");
//        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
//        for (int i : numbers) {
//            System.out.println(i);
//        }
//        System.out.println("Ex 14");
//        for (i = 1; i <= 100; i++) {
//            if (i % 3 == 0) {
//                System.out.println(i);
//            }
//        }
//
//            System.out.println("Ex 14 v 2");
//            for (int i = 3; i <= 99; i += 3){
//                System.out.println(i);
//
//    }
//            System.out.println("Ex 15");
//            String[] bands = {"Sun", "Metsatöll", "Queen", "Metallica"};
//            String bandsText = "\"";
//            for (int i = 0, t < bands.length; i++){
//                bandsText = bandsText + bands[i];
//                //kui ei ole viimane element
//                if (i < bands.length -1) {
//                    bandsText = bandsText + ", ";
//                }
//             }
//            bandsText = bandsText + "\"";
//            System.out.println(bandsText);
//
//            System.out.println("Ex 15 alternative");
//            String bandsTextWithJoin = String.join(", ", bands);
//        System.out.println(bandsTextWithJoin);
//
//
//        System.out.println("Ex 16");
//        String bandsReverseText = "";
//        for(int i = bands.length -1; i>=0; i--){
//            bandsReverseText += bands [1];
//            if (i > 0) {
//                bandsReverseText += ", ";
//            }
//        }
//        System.out.println(bandsReverseText);

        /*System.out.println("Ex 17");
        String numberWords = {"üks", "kaks", "kolm", "neli", "viis", "kuus", "seitse", "kaheksa", "üheksa"};

        String numbersText = "";
        for (int i = 0; i < args.length; i++) {
            *//*switch (args[i]) {
                case "1":
                    numbersText = numbersText + "üks";
                    break;
                case "2":
                    numbersText = numbersText + "kaks";
                    break;
                case "3":
                    numbersText = numbersText + "kolm";
                    break;
                case "4":
                    numbersText = numbersText + "neli";
                    break;
                case "5":
                    numbersText = numbersText + "viis";
                    break;
                case "6":
                    numbersText = numbersText + "kuus";
                    break;
                case "7":
                    numbersText = numbersText + "seitse";
                    break;
                case "8":
                    numbersText = numbersText + "kaheksa";
                    break;
                case "9":
                    numbersText = numbersText + "üheksa";
                    break;
                case "0":
                    numbersText = numbersText + "null";
                    break;*//*

            int inputNumber = Integer.parseInt(args[i]);
            numbersText = numbersText + ", " + numberWords[inputNumber];
        }

        numbersText = numbersText.length() >= 2 ? numbersText.substring(2) : "";
        System.out.println(numbersText);*/


        System.out.println("Ex 18");

        double randomNumber;
        do {
            System.out.println("Tere");
            randomNumber = Math.random();
        } while (randomNumber < 0.5);
    }
}

