package ee.bcs.valiit.day8;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NatureParkManager<getDate> {
    public static void main(String[] args) {
        List<NatureParkGuests> visits = loadData("resources/visits.txt");

//a) sorteerib külastuspäevad külastajate arvu järgi kasvavalt ning prindib tulemuse konsoolile;
        //Variant 1 Comparator
        Collections.sort(visits, (count1, count2) -> count1.getCount() - count2.getCount());
//        System.out.println(visits);


      //Variant 2 Comparable
//        Collections.sort(visits);
        for (NatureParkGuests visit: visits) {
            System.out.println("------------------------------------");
            System.out.println("Kuupäev: " + visit.getDate());
            System.out.println("Külastajaid: " + visit.getCount());
        }

        //b) prindib konsoolile päeva, mil külastajaid oli kõige rohkem.
//        NatureParkGuests lastVisit = visits.get(visits.size() - 1);
// System.out.printf("Kõige rohkem külastajaid oli %s (%d inimest).", lastVisit.getDate(), lastVisit.getCount());

        //kui ei sorteeri läbi listi, aga otsin kõige suuremat külastust, võrdleb igat külastuste arvu
        NatureParkGuests maxVisit = getMaxVisitEntity(visits);
        System.out.printf("Kõige rohkem külastajaid oli %s (%d inimest).", maxVisit.getDate(), maxVisit.getCount());
    }

    private static NatureParkGuests getMaxVisitEntity (List<NatureParkGuests> visits){
        NatureParkGuests max = null;
        for (NatureParkGuests visit : visits) {
            if (max == null) {
                max = visit;
            }else if (visit.getCount() > max.getCount()) {
                max = visit;
            }
        }
        return max;

        //lambda avaldis- kas siin midagi valesti?
        //return visits.stream().max((count1, count2) -> count1.getCount() - count2.getCount()).orElse(null);

    }






    private static List<NatureParkGuests> loadData(String filePath) {
        try{
            List<String> fileLines = Files.readAllLines (Paths.get(filePath));
            //konverteerime stringi Visit objektideks
            List<NatureParkGuests> visits = new ArrayList<>();
            for (String visitString : fileLines) {//käib iga rea läbi
            //teeb uue objekti iga rea kohta
            visits.add(new NatureParkGuests (visitString));//mis on list ja lisab sinna sisse
        }
            return visits;
    } catch (IOException e){
            System.out.println("Mingi jama...");
            e.printStackTrace();
            return null;
        }
    }


}



