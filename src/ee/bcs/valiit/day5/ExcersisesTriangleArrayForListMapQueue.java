package ee.bcs.valiit.day5;

import java.util.*;

public class ExcersisesTriangleArrayForListMapQueue {
    public static void main(String[] args) {
        System.out.println("Ex 1");


        //kahe for tsükliga
        for (int row = 1; row <= 6; row++) {
            for (int col = 1; col <= 6; col++) {
                //Rida 1: 6 (7-rida)
                //Rida 2: 5 (7-rida)
                //Rida 3: 4 (7-rida)
                //Rida 4: 3 (7-rida)
                //Rida 5: 2 (7-rida)
                //Rida 6: 1 (7-rida)
                //
                if (col <= 7 - row) {
                    System.out.print("#");
                }

            }
            System.out.println();
        }


        System.out.println("Ex 2");
        for (int row = 1; row <= 6; row++) {
            for (int col = 1; col <= row; col++) {
                //Rida 1: 1 (7-rida)
                //Rida 2: 2 (7-rida)
                //Rida 3: 3 (7-rida)
                //Rida 4: 4 (7-rida)
                //Rida 5: 5 (7-rida)
                //Rida 6: 6 (7-rida)
                //
                System.out.print("#");
            }
            System.out.println();
        }

        System.out.println("Ex 3 v 1");
//kõigepealt tühike kohapeale nt + märgid ja siis asendada esimeses kolmnurgas tühikutega
        for (int row = 1; row <= 4; row++) {
            //esimene kolmnurk
            for (int col = 1; col <= 5 - row; col++) {
                //Rida 1:
                System.out.print(" ");
            }
            //teine kolmnurk
            for (int col = 1; col <= row; col++) {
                System.out.print("@");
            }
            System.out.println();

        }

//        System.out.println("Ex 3 v 2");
////kõigepealt tühike kohapeale nt + märgid ja siis asendada esimeses kolmnurgas tühikutega
//        for (int row = 1; row <= 5; row++) {
//            //esimene kolmnurk
//          for (int col = 1; col <= 5; col++) {
//                if (col <= 5 - row){
//                    System.out.print("+");
//                }else {
//                    System.out.println("@");
//            }
//            //teine kolmnurk
//            //System.out.print(col <= 5 - row ? " " : "@");
//            }
//
//        }
//        System.out.println();

        System.out.println("Ex 4");
        //Variant 1: for tsükkel
        //1234567 -> 7654321
        //389 -> 983
        int number = 1234567;
        String numberString = String.valueOf(number);//1234567 ->"1234567"

        String reverseNumberString = "";
        //saab tähemärgid teistpidi keerata Integer.reverse keerab kahendväärtused, mitte numbreid
        for (char c : numberString.toCharArray()) {
            reverseNumberString = c + reverseNumberString;
        }
        int result = Integer.parseInt(reverseNumberString);//"7654321"->7654321
        System.out.println(result);

        //Variant 2: StringBuilder
        number = 389;
        numberString = String.valueOf(number);
        StringBuilder stringBuilder = new StringBuilder(numberString);
        reverseNumberString = stringBuilder.reverse().toString();//kõigepealt keerab teistpidi ja siis annab stringi välja
        result = Integer.parseInt(reverseNumberString);
        System.out.println(result);

        System.out.println("Ex 5");
        //loeb käsurealt nime ja punktid
        for (int i = 0; i < args.length; i += 2) {
            String studentName = args[i];
            int studentPoints = Integer.parseInt(args[i + 1]);
            //Map<String, Integer> examResults = new HashMap<>();
            //examResults = args{};
            //System.out.println("Kati: " + examResults.get());
            int grade = 0;
            if (studentPoints < 51) {
                grade = 0;
            } else if (studentPoints < 61) {
                grade = 1;
            } else if (studentPoints < 71) {
                grade = 2;
            } else if (studentPoints < 81) {
                grade = 3;
            } else if (studentPoints < 91) {
                grade = 4;
            } else if (studentPoints < 101) {
                grade = 5;
            } else {
                grade = -1;
            }

            if (grade == 0) {
                System.out.println(String.format("%s: FAIL", studentName));
            } else if (grade > 0) {
                System.out.println(String.format("%s: PASS -%d, %d", studentName, grade, studentPoints));
            } else {
                System.out.println("Uncorrect");
            }
        }

        System.out.println("Ex 6");
        //tegemist on kolmnurgaga
        //kaks arvu igas reas
        //väärtustada
        double[][] triangleSideSet = {
                {56.91, 45.39},
                {23.65, 12.11},
                {98.99, 78.65},
                {123.61, 896.23},
                {456.172, 654.546},
                {349.0, 672.7},
                {3, 4},
        };
        for (int i = 0; i < triangleSideSet.length; i++) {
            double sideA = triangleSideSet[i][0];
            double sideB = triangleSideSet[i][1];
            //täisnurkne kolmnurk a ruut + b ruut
            double sideC = Math.sqrt(Math.pow(sideA, 2) + Math.pow(sideB, 2));
            System.out.println(String.format("Täisnurkne kolmnurk küljepikkustega %f ja %f omab hüpotenuusi pikkusega %f.", sideA, sideB, sideC));

        }

        System.out.println("Ex 7");
//        Estonia, Tallinn, Jüri Ratas
//        Latvia, Riga, Arturs Krišjānis Kariņš
//        Lithuania, Vilnius, Saulius Skvernelis
//        Finland, Helsinki, Sanna Marin
//        Sweden, Stockholm, Stefan Löfven
//        Norway, Oslo, Erna Solberg
//        Denmark, Copenhagen, Mette Frederiksen
//        Russia, Moscow, Mikhail Mishustin
//        Germany, Berlin, Angela Merkel
//        France, Paris, Édouard Philippe
        String[][] countries = {
                {"Estonia", "Tallinn", "Jüri Ratas"},
                {"Latvia", "Riga", "Arturs Krišjānis Kariņš"},
                {"Finland", "Helsinki", "Sanna Marin"},
                {"Sweden", "Stockholm", "Stefan Löfven"},
                {"Norway", "Oslo", "Erna Solberg"},
                {"Denmark", "Copenhagen", "Mette Frederiksen"},
                {"Russia", "Moscow", "Mikhail Mishustinš"},
                {"Germany", "Berlin", "Angela Merkel"},
                {"France", "Paris", "Édouard Philippe"},
        };
        //Variant 1: for-tsükkel
        for (int i = 0; i < countries.length; i++) {
            //System.out.println(countries[i][2]);//i = iga rida ja igast reast 2.element
            //siin peab olema 2dimensiooniline
            System.out.println(String.format("Country: %s, Capital: %s, Prime minister: %s", countries[i][0], countries[1][1], countries[i][2]));
        }

        //Variant 2: foreach tsükkel
        //siin sees on ühetasandiline massiiv
//        for (String [] country:
//             countries) {
//            //System.out.println(country[2]);//saab aru, mis on massiivis 2.element
//            System.out.println(String.format("Country: %s, Capital: %s, Prime minister: %s", country[0], country[1], country[2]));
//        }

        //trüki Country:, Capital:, Prime minister:


        System.out.println("Ex 8");
        //esimene mõõde on read
        //teine mõõde on erinevad lainelised sulud
        //kolmas mõõde on sõnad jutumärkides
        String[][][] countries2 = {
                {{"Estonia"}, {"Tallinn"}, {"Jüri Ratas"}, {"Estonian", "Russian", "Finnish"}},
                {{"Latvia"}, {"Riga"}, {"Arturs Krišjānis Kariņš"}, {"Latvian", "Russian", "Estonian"}},
                {{"Finland"}, {"Helsinki"}, {"Sanna Marin"}, {"Lithuanian", "Polish", "Russian"}},
                {{"Sweden"}, {"Stockholm"}, {"Stefan Löfven"}, {"Swedish", "Danish", "English", "German"}},
                {{"Norway"}, {"Oslo"}, {"Erna Solberg"}, {"Latvian", "Russian", "Estonian"}},
                {{"Denmark"}, {"Copenhagen"}, {"Mette Frederiksen"}, {"Latvian", "Russian", "Estonian"}},
                {{"Russia"}, {"Moscow"}, {"Mikhail Mishustinš"}, {"Latvian", "Russian", "Estonian"}},
                {{"Germany"}, {"Berlin"}, {"Angela Merkel"}, {"Latvian", "Russian", "Estonian"}},
                {{"France"}, {"Paris"}, {"Édouard Philippe"}, {"Latvian", "Russian", "Estonian"}},
        };
        //kolmetasandilise massiivi sees on 2tasandiline massiiv
        for (String[][] country : countries2) {
            String countryName = country[0][0];
            String countryCapital = country[1][0];
            String countryPrimeMinister = country[2][0];
            String[] countryLanguages = country[3];
            //System.out.println(country[0][0]);
            System.out.println(String.format("%s / %s / %s", countryName, countryCapital, countryPrimeMinister));
            for (String countryLanguage : countryLanguages) {
                System.out.println("\t\t" + countryLanguage);
            }
        }

        System.out.println("Ex 9");
        List<List<List<String>>> countries3 =
                Arrays.asList(
                        Arrays.asList(
                                Collections.singletonList("Estonia"),
                                Collections.singletonList("Tallinn"),
                                Collections.singletonList("Jüri Ratas"),
                                Arrays.asList("Estonian", "Russian", "Finnish")

                        ),
                        Arrays.asList(
                                Collections.singletonList("Latvia"),
                                Collections.singletonList("Riga"),
                                Collections.singletonList("Arturs Krišjānis Kariņš"),
                                Arrays.asList("Latvian", "Russian", "Estonian")

                        ),
                        Arrays.asList(
                                Collections.singletonList("Lithuania"),
                                Collections.singletonList("Vilnius"),
                                Collections.singletonList("Saulius Skvernelis"),
                                Arrays.asList("Lithuanian", "Polish", "Russian")
                        )


                );
        for (int i = 0; i < countries3.size(); i++) {
            String countryName = countries3.get(i).get(0).get(0);
            String countryCapital = countries3.get(i).get(1).get(0);
            String countryPrimeMinister = countries3.get(i).get(2).get(0);
            List<String> countryLanguages = countries3.get(i).get(3);
            //println(String.format on sama mis printf
            System.out.printf("%s / %s / %s:\n", countryName, countryCapital, countryPrimeMinister);
            for (int j = 0; j < countryLanguages.size(); j++) {
                System.out.println("\t\t" + countryLanguages.get(j));
            }
        }

        System.out.println("Ex 10");
        Map<String, String[][]> countries4 = new TreeMap<>();
        countries4.put(

                "Estonia",
                new String[][]{{"Tallinn", "Jüri Ratas"}, {"Estonian", "Russian", "Finnish"}}
        );
        countries4.put(
                "Latvia",
                new String[][]{{"Stockholm", "Stefan Löfven"}, {"Swedish", "Danish", "English", "German"}}

        );
        countries4.put(

                "Norway",
                new String[][]{{"Oslo", "Erna Solberg"}, {"Norwegian", "Swedish", "English"}}
        );
        for (String countryName : countries4.keySet()) {
            String countryCapital = countries4.get(countryName)[0][0];
            String countryPrimeMinister = countries4.get(countryName)[0][1];
            String[] countryLanguages = countries4.get(countryName)[1];

            System.out.println(String.format("%s / %s / %s:", countryName, countryCapital, countryPrimeMinister));
            for (String countryLanguage : countryLanguages) {
                System.out.println("\t\t" + countryLanguage);
            }
        }

        System.out.println("Ex 11");
        //Queue sees iga tasand on ka tasand, queue ise on ka list ei ole kolmandat taset vaja
        Queue<String[][]> countries5 = new LinkedList<>();
        countries5.add(new String[][]{{"Estonia"}, {"Tallinn"}, {"Jüri Ratas"},{"Estonian", "Russian", "Finnish"}});
        countries5.add(new String[][]{{"Latvia"}, {"Stockholm"}, {"Stefan Löfven"},{"Swedish", "Danish", "English", "German"}});
        countries5.add(new String[][]{{"Norway"}, {"Oslo"}, {"Erna Solberg"},{"Norwegian", "Swedish", "English"}});

        //queue puhul while meetod kõige parem, saab kasutada polli
        while (!countries5.isEmpty()){
            String[][] country = countries5.poll();
            String countryName = country[0][0];
            String countryCapital = country[1][0];
            String countryPrimeMinister = country[2][0];
            String[] countryLanguages = country[3];
            System.out.println(String.format("%s / %s / %s", countryName, countryCapital, countryPrimeMinister));

            for (String countryLanguage : countryLanguages) {
                System.out.println("\t\t" + countryLanguage);
            }
        }

//        for (String[][] country : countries5) {
//            String countryName = country[0][0];
//            String countryCapital = country[1][0];
//            String countryPrimeMinister = country[2][0];
//            String[] countryLanguages = country[3];
//            System.out.println(String.format("%s / %s / %s", countryName, countryCapital, countryPrimeMinister));
//
//            for (String countryLanguage : countryLanguages) {
//                System.out.println("\t\t" + countryLanguage);
//                }
//            }

    }
}



