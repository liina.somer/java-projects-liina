package ee.bcs.valiit.day7;

public class DatabaseConnectionManager {
    public static void main(String[] args) {
        DatabaseConnection connection1 = DatabaseConnection.getConnection();
        DatabaseConnection connection2 = DatabaseConnection.getConnection();
        DatabaseConnection connection3 = DatabaseConnection.getConnection();
    }
}
