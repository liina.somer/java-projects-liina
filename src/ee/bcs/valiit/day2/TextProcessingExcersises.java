package ee.bcs.valiit.day2;

public class TextProcessingExcersises {
    public static void main(String[] args) {
        /*Ülesanne 1
Hello, World!
Hello "World"!
Steven Hawking once said: "Life would be tragic if it weren't funny".
Kui liita kokku sõned "See on teksti esimene pool  " ning "See on teksti teine pool", siis tulemuseks saame "See on teksti esimene pool See on teksti teine pool".
Elu on ilus.
Elu on 'ilus'.
Elu on "ilus".
Kõige rohkem segadust tekitab "-märgi kasutamine sõne sees.
Eesti keele kõige ilusam lause on: "Sõida tasa üle silla!"
'Kolm' - kolm, 'neli' - neli, "viis" - viis.
*/

        System.out.println("Ex1");
        System.out.println("Hello, World!");
        System.out.println("Hello, \"World\"!");

        String stevenQuote = "Steven Hawking once said: \"Life would be tragic if it weren't funny\".";
        System.out.println(stevenQuote);

        String complexSentence = "Kui liita kokku sõned \"See on teksti esimene pool  \" ning \"See on teksti teine pool\", siis tulemuseks saame \"See on teksti esimene pool See on teksti teine pool\".\"";
        System.out.println(complexSentence);

        System.out.println("Elu on ilus.");
        System.out.println("Elu on 'ilus'.");
        System.out.println("Elu on \"ilus\".");
        System.out.println("Kõige rohkem segadust tekitab \"-märgi kasutamine sõne sees.");
        String mostBeautiful = "Eesti keele kõige ilusam lause on: \"Sõida tasa üle silla!\"";
        System.out.println(mostBeautiful);
        System.out.println("'Kolm' - kolm, 'neli' - neli, \"viis\" - viis.");

         System.out.println ("Ex2");
         /*Ülesanne 2
Defineeri String-tüüpi muutuja tallinnPopulation​. ​Anna muutujale väärtus "450 000".
Kirjuta konsoolile järgmine lause: "Tallinnas elab 450 000 inimest"​, kus rahvaarvu number pärineb muutujast tallinnPopulation​.
Defineeri muutuja populationOfTallinn​ täisarvuna. Prindi konsoolile sama lause, mis punktis 2, kasutades muutujat populationOfTallinn​.*/
         String tallinnPopulation = "450 000";
         String populationText1 = "Tallinnas elab " + tallinnPopulation;
         System.out.println(populationText1);

         //%s- kasutab seal vahel stringi, place holdereid on erinevaid
         String populationText2 = String.format("Tallinnas elab %s inimest!", tallinnPopulation);
         System.out.println(populationText2);
         int populationOfTallinn = 450_000;
         //%,d tuhandete eraldaja, võtab tühiku kui arvuti op süsteemi regional settings all on nii määratud
         String populationText3 = String.format("Tallinnas elab %,d inimest!", populationOfTallinn);
        System.out.println(populationText3);

/*Ülesanne 3
Defineeri muutuja bookTitle​.
Omista muutujale väärtus "Rehepapp".
Prindi konsoolile tekst: "Raamatu "Rehepapp" autor on Andrus Kivirähk"​, kus raamatu nimi pärineb muutujast bookTitle​.*/
        System.out.println("Ex3");
        String bookTitle = "Rehepapp";
        String bookText = String.format("Raamatu \"%s\" autor on Andrus Kivirähk.", bookTitle);
        System.out.println(bookText);
/*Ülesanne 4
Defineeri muutujad
planet1 ​väärtusega "Merkuur"
planet2 ​väärtusega "Venus"
planet3 ​väärtusega "Maa"
planet4 ​väärtusega "Marss"
planet5 ​väärtusega "Jupiter"
planet6 ​väärtusega "Saturn"
planet7 ​väärtusega "Uran"
planet8 ​väärtusega "Neptuun"
planetCount​ väärtusega 8
Prindi standardväljundisse lause: "Merkuur, Veenus, Maa, Marss, Jupiter, Saturn, Uraan ja Neptuun on Päikesesüsteemi 8 planeeti"​, kasutades eelnevalt defineeritud muutujaid.
Prindi standardväljundisse sama lause, aga kasuta seejuures abifunktsiooni String.format()​.
*/
        System.out.println("Ex4");
        String planet1 = "Merkuur";
        String planet2 = "Venus";
        String planet3 = "Maa";
        String planet4 = "Marss";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uran";
        String planet8 = "Neptuun";
        int planetCount = 8;
        //String planetText = "planet1 + ", " + planet2 + ", "+ planet3 + ", "+ planet4 ", "+ planet5 + ", " + planet6 ", " + planet7 + " ja " + planet8 + "on Päikesesüsteemi" + planetCount + " planeeti"";
        //System.out.println(planetText);
        System.out.println("\"Merkuur, Veenus, Maa, Marss, Jupiter, Saturn, Uraan ja Neptuun on Päikesesüsteemi " + planetCount + " planeeti\"");
        String allEigthPlanets = String.format("%s, %s, %s, %s, %s, %s, %s ja %s on Päikesesüsteemi %d planeeti.",
                planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8, planetCount);
        System.out.println(allEigthPlanets);


    }
}
