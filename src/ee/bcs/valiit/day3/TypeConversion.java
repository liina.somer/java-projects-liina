package ee.bcs.valiit.day3;

public class TypeConversion {
    public static void main(String[] args) {
        int numberA = 123;
        String textA = String.valueOf(numberA);
        System.out.println(textA);

        boolean boolB = true;
        String textB = String.valueOf(boolB);
        System.out.println(textB);

        //primitiivid väikse tähega, igal tüübil oma wrapper ja see
        //pole enam primitiiv ja see suure tähega
        String textC = "false";
        boolean boolC = Boolean.parseBoolean(textC);
        System.out.println(boolC);

        String textD = "123456";
        int numberD = Integer.parseInt(textD);
        System.out.println(numberD);

        Integer i = 777;//see pole primitiiv
        byte b = i.byteValue();//00001001 == 9 DEC
        System.out.println(b);

        /*int i = 777;//seda saab ainult primitiividega teha
        //eelmist varianti i.byteValue() ei saa primitiividega
        byte b = (byte)i;//type casting, ehk siis jõuga surub sinna
        System.out.println(b);*/

        double d = 123.456;
        //või String textDouble = String.valueOf(d);
        String textDouble = ((Double)d).toString();
        System.out.println(textDouble);

    }
}
