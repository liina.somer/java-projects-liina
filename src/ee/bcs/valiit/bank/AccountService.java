package ee.bcs.valiit.bank;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AccountService {

    //list accountidest, mitte stringidest, muidu ei saa Accounti kasutada, aga me tahame manipuleerida Accountidega
    //peab olema static, et oleks klassi jaoks olemas ja tagapool näha, temast objekti ei tehta, koopiana kaasa ei panda

    private static List<Account> accounts = new ArrayList<>();//siia sisse saab hakata panema
//on oht, et viskab exceptioneid, me viskame selle edasi main meetodisse, mis viskab selle edasi virtuaalmasinale
    //virtuaalmasin tegeleb sellega, mitte ei lõpeta exceptioni esinemisel tööd

    public static void loadAccounts (String accountFilePath) throws IOException {
        //tekitame konto objektid ja paneme need listi
//loeb sisse ja tekitab igast elemendist stringide listi

        List<String> accountCsvs = Files.readAllLines (Paths.get(accountFilePath));
//        for (String accountCsv : accountCsvs) {//käib iga rea läbi
//            Account acc = new Account(accountCsv);//Account konstruktor on, teeb uue objekti iga rea kohta
//            accounts.add(acc);//mis on list ja lisab sinna sisse
//        }

        //saab kas for tsükliga, mis siin ees või siis selle lamda avaldisega:
        accounts = accountCsvs.stream().map(Account::new).collect(Collectors.toList());
    }

    public static Account findAccount (String accountNumber) {
        //otsime accounts listist sobiva account objekti ja tagastame selle
        //läbi otsimiseks for või foreach kõige parem
        for (Account account : accounts) {
            if (account.getAccountNumber().equals(accountNumber)) {
                return account;
            }
        }
        return null;

    }

public static Account findAccount (String firstName, String lastName) {
    for (Account account : accounts) {
        if (account.getFirstName().equalsIgnoreCase(firstName) &&
                account.getLastName().equalsIgnoreCase(lastName)) {
            return account;
        }
    }
    return null;
}
//lamda avaldisega saab ka
//      List<Account> filteredAccounts = accounts
//              .stream().filter(acc-> acc.getFirstName().equalsIgnoreCase(firstName) && acc.getLastName().equalsIgnoreCase(lastName))
//              .collect(Collectors.toList());
//      return filteredAccounts.size() > 0 ? filteredAccounts.get(0) : null;


    public static TransferResult transfer(String fromAccountNumber, String toAccountNumber, double sum) {
       Account from = findAccount(fromAccountNumber);//taaskasutame findAccount funktsiooni, mis on eespool
       Account to = findAccount(toAccountNumber);

       //Valideerimine(kas saab üldse ülekannet teha?)
    if (from == null){
        return new TransferResult(false, "Maksja kontot ei eksisteeri");
    } else if (to == null) {
        return new TransferResult(false, "Saaja kontot ei eksisteeri");
    } else if (!(sum > 0)) {
        return new TransferResult(false, "Ülekantav summa peab olema positiivne");
    } else if (from.getBalance() < sum ) {
        return new TransferResult(false, "Puuduvad piisavad vahendid");
    }

//kõik on eelnevalt kontrollitud, nüüd saab ülekande ära teha
    from.setBalance(from.getBalance() - sum);
    to.setBalance(to.getBalance() + sum);
    return new TransferResult(true, "OK", from, to);
           }

    public static List<Account> getAccounts() {
        return accounts;
    }
}



