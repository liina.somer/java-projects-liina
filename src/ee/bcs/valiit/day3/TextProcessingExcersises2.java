package ee.bcs.valiit.day3;

import java.util.Scanner;

public class TextProcessingExcersises2 {
    public static void main(String[] args) {
        /*
Ülesanne 1
Defineeri muutujad
president1 ​väärtusega "Konstantin Päts"
president2 ​väärtusega "Lennart Meri"
president3 ​väärtusega "Arnold Rüütel"
president4 ​väärtusega "Toomas Hendrik Ilves"
president5 ​väärtusega "Kersti Kaljulaid"
Defineeri String-tüüpi muutuja ja pane see viitama järgmisele tekstile:

"Konstantin Päts, Lennart Meri, Arnold Rüütel, Toomas Hendrik Ilves ja Kersti Kaljulaid on Eesti presidendid."​

Kasuta lause konstrueerimiseks StringBuilder klassi.
Prindi tekst konsoolile.*/

        System.out.println("Ex 1");
        String president1 = "Konstantin Päts";
        String president2 = "Lennart Meri";
        String president3 = "Arnold Rüütel";
        String president4 = "Toomas Hendrik Ilves";
        String president5 = "Kersti Kaljulaid";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(president1);
        stringBuilder.append(", ");
        stringBuilder.append(president2);
        stringBuilder.append(", ");
        stringBuilder.append(president3);
        stringBuilder.append(", ");
        stringBuilder.append(president4);
        stringBuilder.append(" ja ");
        stringBuilder.append(president5);
        stringBuilder.append(" on Eesti presidendid.");
        System.out.println(stringBuilder);

        System.out.println("Ex 2");
/*Ülesanne 2
Defineeri String-tüüpi muutuja ja pane see viitama tekstile

"Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida."

Kasutades Scanner klassi, prindi konsoolile järgmised read:

See on esimene rida.
See on teine rida.
See on kolmas rida.*/


        String text = "Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.";

        Scanner myScanner = new Scanner(text);
        myScanner.useDelimiter("Rida: ");

        System.out.println(myScanner.next());
        System.out.println(myScanner.next());
        System.out.println(myScanner.next());






    }
}
