package ee.bcs.valiit.testEx;

import java.util.Arrays;

public class Words {
    public static void main(String[] args) {
        System.out.println("Censored words: ");
        String[] words = {"auto", "rebane", "sau"};
        String[] censoredWords = censorWords(words);
        for (String censoredWord : censoredWords) {
            System.out.println(" " + censoredWord);
        }
    }


        private static String[] censorWords (String[]words){
            String[] response = new String[words.length];

            for (int i = 0; i < response.length; i++) {
                response[i] = censorWord(words[i]);
            }

            return response;
        }

        private static String censorWord (String word){
            String result = "";
            for (int i = 0; i < word.length(); i++) {
                result += "#";
            }
            return result;
        }
    }
