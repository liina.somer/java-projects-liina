package ee.bcs.valiit.day4;

import java.lang.reflect.Array;
import java.net.PortUnreachableException;
import java.util.*;
import java.util.stream.Stream;

public class CollectionsExcersises {
    public static void main(String[] args) {

        System.out.println("Ex 19");
    /*    List<String> cityNames = new ArrayList<>();
        cityNames.add("Tallinn");
        cityNames.add("Tartu");
        cityNames.add("Pärnu");
        cityNames.add("Jõhvi");
        cityNames.add("Haapsalu");*/

        //nii ei saa listi muuta, ei saa lisada, aga nii saab inline meetodil
        List<String> cityNames = Arrays.asList("Tallinn", "Tartu", "Pärnu", "Jõhvi", "Haapsalu");
        //nii saab ka lisada, kui tekitada uus list
        //List<String> cityNames = new ArrayList("Tallinn", "Tartu", "Pärnu", "Jõhvi", "Haapsalu");
        //cityNames.add("Baden-baden");
        System.out.println("Esimene linn: " + cityNames.get(0));
        System.out.println("Kolmas linn: " + cityNames.get(2));
        System.out.println("Viimane linn: " + cityNames.get(cityNames.size() - 1));

        System.out.println("Ex 20");

        Queue<String> namesQueue = new LinkedList<>();
        namesQueue.add("Toomas");
        namesQueue.add("Juku");
        namesQueue.add("Karl");
        namesQueue.add("Ott");
        namesQueue.add("Susanna");
        namesQueue.add("Lily");
        //võib ka (namesQueue.size() > 0) või (namesQueue.peek() != 0)
        while (!namesQueue.isEmpty()) {
            System.out.println(namesQueue.remove());
            //enamasti ei tea, mitu inimest järjekorras, tasuks kasutada while tsüklit
//            System.out.println(namesQueue.poll());
//            System.out.println(namesQueue.poll());
//            System.out.println(namesQueue.poll());
//            System.out.println(namesQueue.poll());
//            System.out.println(namesQueue.poll());
//            System.out.println(namesQueue.poll());
        }
            System.out.println("Ex 21");
        Set<String> namesSet = new TreeSet<>();
        namesSet.add("Toomas");
        namesSet.add("Juku");
        namesSet.add("Karl");
        namesSet.add("Ott");
        namesSet.add("Susanna");
        namesSet.add("Lily");
        //rakenda igale elemenendile sellist funktsiooni:
       // namesSet.forEach(name -> System.out.println(name));
       namesSet.forEach(System.out::println);

        System.out.println("Ex 22");
        Map<String, String[]> citiesOfCountries = new HashMap<>();
//        String[] estoniaCities = {"Tallinn", "Tartu", "Valga", "Võru"};
//        citiesOfCountries.put("Estonia", estoniaCities);
//        String[] swedenCities = {"Stockholm", "Uppsala", "Lund", "Köping"};
//        citiesOfCountries.put("Sweden", swedenCities);
//        String[] finlandCities = {"Helsinki", "Espoo", "Hanko", "Jämsä"};
//        citiesOfCountries.put("Finland" + finlandCities);
//        System.out.println(citiesOfCountries);

        Map <String, List<String>> countries2 = new HashMap<>();
        countries2.put("Estonia", Arrays.asList("Tallinn", "Tartu", "Valga", "Võru"));
        countries2.put("Sweden", Arrays.asList("Stockholm", "Uppsala", "Lund", "Köping"));
        countries2.put("Finland", Arrays.asList("Helsinki", "Espoo", "Hanko", "Jämsä"));
        System.out.println(countries2);

        for (String country : countries2.keySet()){
            System.out.println("Country: " + country);
            System.out.println("Cities: ");
            //anna mulle selle mapi seest linnad kohal riik, sama kui kirjutaks sinna riigi,
            // //antud juhul ta võtab iga tsükli ajal linnad kohal uus riik
            //iga tsükli korral pannaks uus linn riigi
            for (String city : countries2.get(country)){
                System.out.println("\t" + city);}//"\t" on taane "\t\t" topelttaane

//        countries2.forEach(country, citiesOfCountries) -> {
//            System.out.println("Country: " + country);
//                System.out.println("Cities: ");
//                Stream.of(citiesOfCountries).forEach(countryCity -> System.out.println("\t\"));
//        };


        }
    }
}
