package ee.bcs.valiit.testEx;

public class Temperatures {
    public static void main(String[] args) {
        int temperature1 = 25;
        int temperature2 = 31;

        System.out.printf("Ilm: %s\n", whatWeather(temperature1));
        System.out.printf("Ilm: %s\n", whatWeather(temperature2));
    }
    private static String whatWeather (int temperature) {
        if (temperature > 30) {
            return "Leitsak";
        } else if (temperature > 25) {
            return "Rannailm";
        } else if (temperature > 21) {
            return "Mõnus suveilm";
        } else if (temperature > 15) {
            return "Jahe suveilm";
        } else if (temperature > 10) {
            return "Jaani-ilm";
        } else if (temperature > 0){
            return "Telekavaatamise ilm";
        } else return "Suusailm";


        }
    }



