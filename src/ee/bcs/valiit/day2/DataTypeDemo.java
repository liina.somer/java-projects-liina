package ee.bcs.valiit.day2;

public class DataTypeDemo {
    public static void main(String[] args) {
        //1 - byte (1 bait)
        byte mySmallNumber = 127; //siin peab ka väärtus olema, muidu intellij annab errori
        System.out.println(mySmallNumber);

        short myMediumNumber = 10000;
        System.out.println(myMediumNumber);

        int myRelativelyLargeNumber = 1_000_000_000;
        System.out.println(myRelativelyLargeNumber);
        //long 8 baiti
        long myVeryLargeNumber = 200_000_000_000_000_000L;
        System.out.println(myVeryLargeNumber);
        //long 4 baiti
        float myDecimal1 = 656.545362987F;
        System.out.println(myDecimal1);
        //double 8 baiti
        double myDecimal4 = 4.8D;
        double myDecimal5 = 5.6D;
        System.out.println(myDecimal4 + myDecimal5);
        // char on täht, 2 baiti
        char myChar1 = '2';
        short myChar3 = 116;
        System.out.println((short)myChar1);

        //boolean(1bait)
        boolean isEarthRound = true || false && true; //1 + 0 * 1, korrutamine prioriteetsem
        System.out.println(isEarthRound);

        //wrapper classes
        Long wrapperA = 5L;
        Long wrapperB =8L;
        long wrapperSum = wrapperA + wrapperB;
        System.out.println(wrapperSum);

    }
}
