package ee.bcs.valiit.day6;

public class Pet {

    //need muutujad ei kuulu klassile
    //need muutujad kuuluvad objektile, kuna pole static
    public String name;
    public int ageInMonths;
    public String color;
    public double weight;
    public String description;
    public boolean isVaccinated;

    //see muutuja kuulub klassile
    //seda teine klass ei vaata
    public static int petCount = 0;


    //this- see objekt, mille sisse ma kuulun
    //kui see oleks static, siis ta ei saa aru, mis see this.name on
    public void introduce() {
        System.out.println("Mina olen " + this.name);
    }
}
