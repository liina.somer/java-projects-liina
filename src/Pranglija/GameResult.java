package Pranglija;

public class GameResult {
    //instruktsioon objekti loomiseks, static´ut ei tohi kasutada
    //et kui kunagi teen mingi konteineri, siis see peab olema selliste sahtlitega
    public String name;
    public double time;
    public String numberRange;

    //parem klikk generate, toString(), kõik väljad OK
    @Override
    public String toString() {
        return "GameResult{" +
                "name='" + name + '\'' +
                ", time=" + time +
                ", numberRange='" + numberRange + '\'' +
                '}';
    }
}
