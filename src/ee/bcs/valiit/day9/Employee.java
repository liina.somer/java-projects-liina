package ee.bcs.valiit.day9;

import java.util.Objects;

public class Employee implements Comparable<Employee> {
    private String firstName;
    private String lastName;
    private int salary;
    private String nationality;

    public Employee(String firstName, String lastName, int salary, String nationality) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
        this.nationality = nationality;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", salary=" + salary +
                ", nationality='" + nationality + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return salary == employee.salary &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(nationality, employee.nationality);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, salary, nationality);
    }

    @Override
    public int compareTo(Employee another) {

        //sorteerime palgataseme, eesnime ja perenime järgi kasvavalt
        if (this.salary - another.salary !=0){
            return this.salary - another.salary;
    }
        else if (this.firstName.compareTo(another.firstName) !=0) {
            return this.firstName.compareTo(another.firstName);
        }else {
            return this.lastName.compareTo(another.lastName);
        }




        //kui mina pean olema eespool kui another, siis tagastan negatiivse arvu(-1, -567, -2345)
        //kui mina pean olema tagapool kui another, siis tagastan positiivse(1, 67, 7890)
        //kui mina olen võrdne another-iga, siis tagasta 0

        //kui priidust alustada siis korda -1
       // return this.firstName.compareTo(another.firstName * -1);

        //nii saab väga kergelt sorteerida palkasid, kui tahta teistpidi keerata, siis another enne
        //return this.salary - another.salary;
        //sorteerime palgataseme järgi kasvavalt
        //if lausega saab ilusti
//        if (this.salary > another.salary){
//        return 1
//    } else if (this.salary < another.salary) {
//            return -1;
//        }else {
//            return 0;
//        }
    }
}
