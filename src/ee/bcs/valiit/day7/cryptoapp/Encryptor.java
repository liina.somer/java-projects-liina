package ee.bcs.valiit.day7.cryptoapp;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Encryptor extends Cryptor {
    public Encryptor(String alphabetFilePath) throws IOException {
        super(alphabetFilePath); //super klassist välja kutsumine
    }

    @Override
    protected Map<String, String> getAlphabetMap(List<String> alphabetLines) {
        Map<String, String> alphabetMap = new HashMap<>();
        for (String line : alphabetLines) {
            String[] lineParts = line.split(", ");
            alphabetMap.put(lineParts[0], lineParts[1]);
        }
        return alphabetMap;
    }
}
