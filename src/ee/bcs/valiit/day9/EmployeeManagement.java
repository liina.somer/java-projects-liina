package ee.bcs.valiit.day9;

import java.util.*;

public class EmployeeManagement {
    public static void main(String[] args) {
        Employee mati = new Employee("Mati", "Kask", 1500, "Eestlane");
        Employee kati = new Employee("Kati", "Tamm", 1800, "Rootslane");
        Employee priit = new Employee("Priit", "Järv", 1300, "Soomlane");
        Employee adalbert1 = new Employee("Adalbert", "Mägi", 1200, "Lätlane");
        Employee adalbert2 = new Employee("Adalbert", "Mägi", 1100, "Lätlane");

//        Set<Employee> employeesSet = new HashSet<>();
//        employeesSet.add(mati);
//        employeesSet.add(kati);
//        employeesSet.add(priit);
//        employeesSet.add(adalbert1);
//        employeesSet.add(adalbert2);

//        System.out.println(employeesSet);


        //nii tuleb ka false, kuna kaks eraldi objekti, peab override tegema Employee klassis, siis tuleb true
//      System.out.println(adalbert1.equals(adalbert2));

        //see annab true, kuna võrreldakse eesnimesid
//        System.out.println(adalbert1.getFirstName().equals(adalbert2.getFirstName()));

        //kaks erinevat mälupiirkonda, sellepärast false, peab override tegema
//        System.out.println(adalbert1 == adalbert2);

        List<Employee> employees = new ArrayList<>();
        //prindib selles järjekorras nagu panen
        employees.add(mati);
        employees.add(kati);
        employees.add(priit);
        employees.add(adalbert1);
        employees.add(adalbert2);
        //variant 1: klassikaline
        //Collections.sort(employees, new Comparator<Employee>()

        //variant 2: interface inline
//            Comparator<Employee> empComparator = new Comparator<Employee>() {
//
//            @Override
//            public int compare(Employee employee1, Employee employee2) {
//                return employee2.getSalary() - employee1.getSalary();
//            }
//        };
        // variant 3 lambda avaldis
        Collections.sort(employees, (employee1, employee2) -> employee1.getSalary() - employee2.getSalary());


        //variant 4
//        Collections.sort(employees, Comparator.comparingInt(Employee::getSalary));


        //lambda avaldis interface´i inline realiseering
        // lambda avaldist saab kasutada vaid siis kui interface on @FunctionalInterface
        //functional interface- ainult üks implementeerimist vajav meetod

      System.out.println(employees);
  }
}
