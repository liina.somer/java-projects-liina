package ee.bcs.valiit.day3;

import java.util.Arrays;

public class ArrayExcersises {
    public static void main(String[] args) {
        /*Ülesanne 7
Defineeri muutuja, mille tüübiks täisarvude massiiv.
Loo massiiv pikkusega 5 ja pane oma eelnevalt defineeritud muutuja sellele viitama.
Sisesta ükshaaval massiivi väärtused 1, 2, 3, 4, 5.
Prindi konsoolile esimese massiivielemendi väärtus.
Prindi konsoolile kolmanda massiivielemendi väärtus.
Prindi konsoolile viimase massiivielemendi väärtus.*/
        System.out.println("Ex 7");
        //võib ka int[] myArray;
        //myArray = new int[5];
        int[] myArray = new int[5];
        myArray[0] = 1;
        myArray[1] = 2;
        myArray[2] = 3;
        myArray[3] = 4;
        myArray[4] = 5;

        System.out.println("Esimene element: " + myArray[0]);
        System.out.println("Kolmas element: " + myArray[2]);
        //võib ka viimase puhul + myArray[myArray.length -1]
        System.out.println("Viimane element: " + myArray[4]);

/*Ülesanne 8
Defineeri muutuja, mille tüübiks tekstide massiiv.
Väärtusta see massiiv loomise hetkel väärtustega "Tallinn",
"Helsinki", "Madrid", "Paris".
 */
        System.out.println("Ex 8");
        //inline
        String[] cities = {"Tallinn", "Helsinki", "Madrid", "Paris"};

        //võib ka
        String[] cities2 = new String[4];
        cities2[0] = "Tallinn";
        cities2[1] = "Helsinki";
        cities2[2] = "Madrid";
        cities2[3] = "Paris";
        System.out.println(Arrays.toString(cities2));

/*
Ülesanne 9
Defineeri kahetasandliline massiiv:
Element 1: 1, 2, 3
Element 2: 4, 5, 6
Element 3: 7, 8, 9, 0
*/
        System.out.println("Ex 9");
        int [][] myElements = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9, 0}
        };
       /* int myElements2 = new int[3][];
        myElements2[0] = new int [3];
        myElements2[0][0] = 1;
        myElements2[0][1] = 2;
        myElements2[0][2] = 3;
        myElements2[1] = new int [3];
        myElements2[1][0] = 4;
        myElements2[1][1] = 5;
        myElements2[1][2] = 6;
        myElements2[2] = new int [3];*/
        //jne


        /*Ülesanne 10
Defineeri kahetasandliline massiiv, milles hoida järgmisi
linnu riikide kaupa:
Tallinn, Tartu, Valga, Võru
Stockholm, Uppsala, Lund, Köping
Helsinki, Espoo, Hanko, Jämsä
Väärtusta see massiiv kahel erineval viisil:
Element-haaval
Inline-põhimõttel*/
        System.out.println("Ex 10");
        String [][] countryCities = {
                {"Tallinn", "Tartu", "Valga", "Võru", "Kohila"},
                {"Stockholm", "Uppsala", "Lund", "Köping"},
                {"Helsinki", "Espoo", "Hanko", "Jämsä"},

        };
        /*String countryCities2 = new String[3][4];
        //3 riiki ehk rida ja 4 tulpa ehk linna
        //kui ühes riigis rohkem linnu, siis saab
        //selle eraldi defineerida
        countryCities2[0] = new String[5];
        countryCities2[0][0]= "Tallinn";
        countryCities2[0][1]= "Tartu";
        countryCities2[0][2]= "Valga";
        countryCities2[0][0]= "Võru";
        countryCities2[1][0]= "Stockholm";
        countryCities2[1][1]= "Uppsala";
        countryCities2[1][2]= "Lund";
        countryCities2[2][0]= "Helsinki";
        countryCities2[2][1]= "Espoo";
        countryCities2[2][2]= "Hanko";
        countryCities2[2][3]= "Jämsä";*/


    }
}
