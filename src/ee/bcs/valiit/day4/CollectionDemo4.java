package ee.bcs.valiit.day4;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CollectionDemo4 {
    public static void main(String[] args) {

/*        Set<String> namesSet = new HashSet<>();
        namesSet.add("Malle");
        namesSet.add("Kalle");
        namesSet.add("Gustav");
        namesSet.add("Joosep");


        //prindi välja teine element
        //System.out.println(namesSet.toArray()[1]);
        Object[] names = namesSet.toArray(String[]::new);//::new konstruktor funktsiooni pointer,
        // palun loo mulle massiiv ja kasuta seda konstruktorit, mis asub sellises kohas
        System.out.println(names[1]);

        for (String setElement : namesSet) {
            System.out.println("FOR EACH ELEMENT: " + setElement);
        }

        //Map
        Map<String, String> voroEstDictionary = new HashMap<>();
        voroEstDictionary.put("putsunutsutaja", "tolmuimeja");
        voroEstDictionary.put("rüperaal", "sülearvuti");
        System.out.println("Putsunutsutaja eesti keeles: " + voroEstDictionary.get("putsunutsutaja"));
        System.out.println("Kas putsunutsutaja olemas?" + voroEstDictionary.containsKey("xxx"));*/

        Map<String, Integer> ages = new HashMap<>();

        ages.put("Mati", 25);
        ages.put("Kati", 30);
        ages.put("Tõnu", 35);
        ages.put("Gert", 35);

        System.out.println(ages);

        //see kõige rohkem kasutatav
        System.out.println("key - value väljaprint...");
        System.out.println (ages.keySet());
              for (String name : ages.keySet()) {
                  System.out.println(String.format("%s on %d aastat vana.", name, ages.get(name)));
              }
        System.out.println("values() funktsioon...");
              System.out.println(ages.values());

        System.out.println("entrySet() funktsioon...");
        System.out.println(ages.entrySet());
        for (Map.Entry<String, Integer> entry: ages.entrySet()){
            System.out.println("Entry...");
            System.out.println(entry);

        }


    }
}
