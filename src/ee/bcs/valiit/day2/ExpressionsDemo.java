package ee.bcs.valiit.day2;

public class ExpressionsDemo {
    public static void main(String[] args) {
        //arithmetic expressions
        int a = 1;
        int b = 2;
        int c = a + b - 3 * b;
        System.out.println(c);

        a++;
        System.out.println(a);

        a--;
        System.out.println(a);

        int d = ++a;
        System.out.println(d);

        int e = 12;
        e += 3;
        e *=3;
        System.out.println(e);

        int f = 72;
        f = f % 10; //defineerida saab ainult korra, ehk siin inti enam ei tule

        System.out.println(f);

            //5!=5 mittevõrdsuse kontroll
        boolean isCorrect = 5 != 5; // 5==6 võrdsuse kontroll tehakse enne, see kõrgema prioriteediga, 5!
        isCorrect = 5==5; //muudab eelmise ära
        System.out.println(isCorrect);

    }
}
