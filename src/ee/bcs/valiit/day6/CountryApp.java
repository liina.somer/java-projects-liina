package ee.bcs.valiit.day6;

//import java.util.Arrays;
//
//public class CountryApp {
//    public static void main(String[] args) {
//
//        //ülesanne 2 moodi, andmed konstruktori parameetritena
//        CountryInfo estonia = new CountryInfo("Eesti", "Tallinn", "Jüri Ratas", new String[] {"Estonian", "Russian", "Finnish"});
////        estonia.countryName = "Eesti";
////        estonia.capital = "Tallinn";
////        estonia.primeMinister = "Jüri Ratas";
////        estonia.languages = new String[] {"Estonian", "Russian", "Finnish"};
//
//        CountryInfo latvia = new CountryInfo();
//        latvia.countryName = "Läti";
//        latvia.capital = "Riia";
//        latvia.primeMinister = "Arturs Krišjānis Kariņš";
//        latvia.languages = new String[] {"Latvian", "Russian", "Estonian"};
//
//        CountryInfo lithuania = new CountryInfo();
//        lithuania.countryName = "Leedu";
//        lithuania.capital = "Vilnius";
//        lithuania.primeMinister = "Saulius Skvernelis";
//        lithuania.languages = new String[] {"Lithuanian", "Russian", "Polish"};
//
//
//        CountryInfo[] balticStates = new CountryInfo[3];
//        balticStates[0] = estonia;
//        balticStates[1] = latvia;
//        balticStates[2] = lithuania;
//
//        System.out.println(Arrays.toString(balticStates));
//    }
//}
