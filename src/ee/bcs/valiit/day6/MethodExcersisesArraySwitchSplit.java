package ee.bcs.valiit.day6;

import javax.swing.*;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MethodExcersisesArraySwitchSplit {
    public static void main(String[] args) {

        //main(args);//saab kutsuda iseennast välja, aga see lihtsalt jookseb väga palju kordi ja siis virtuaalmasin annab veateate
        System.out.println("Ex 1");
        test(3);

        System.out.println("Ex 2");
        test2 ("tere", "head aega");

        System.out.println("Ex 3");
        System.out.println(addVat(100.00));

        System.out.println("Ex 4");
        int[] myEx4Array = composeArray(4, 5, false);
        System.out.println(Arrays.toString(myEx4Array));

        System.out.println("Ex 5");
        printTere();

        System.out.println("Ex 6");
        String gender = deriveGender("49403136526");
        System.out.println(gender);

        System.out.println("Ex 7");
        //personalCode siin ei tea midagi allpool defineeritust
        String personalCode1 = "61107121760";
        int birthYear1 = retrieveBirthYear(personalCode1);//annan kaasa väärtuse, mitte muutujat ennast
        System.out.printf("Isikukood: %s, sünniaasta: %s\n", personalCode1, birthYear1);


        System.out.println("Ex 8");
        String personalCode2 = "49403136526";

        System.out.printf("Kas isikukood: %s on korrektne? %s\n", personalCode2, validatePersonalCode(personalCode2));

        System.out.println("Rekursioon");

       aFunction("2"); //saab põhimõtteliselt objecti võtta
       aFunction(456);
       aFunction(true);
    }

    public static void aFunction (Object input) {
        System.out.println(input);
    }
    private static boolean test (int number) {
        return true;
    }
    private static void test2(String tere, String head_aega) {
    }

    private static double addVat (double netPrice) {
        return 1.2 * netPrice;
    }

    //tagastab täisarvude massiivi
private static int[] composeArray (int number1, int number2, boolean duplicate) {
        if (duplicate){ //kui duplicate on true
            return new int[] {number1 * 2, number2 * 2};
        }
        return new int[] {number1, number2};
}
private static void printTere () {
    System.out.println("Tere");
}

private static String deriveGender (String personalCode) {
    int firstDigit = Integer.parseInt(personalCode.substring(0, 1));// nr 4
   // int birthYear = Integer.parseInt(personalCode.substring(1, 3));// nr 94
    String gender;
    return firstDigit % 2 == 1 ? "M" : "F";

//    switch (firstDigit) {
//        case 1:
//        case 3 :
//        case 5 :
//        case 7 :
//            return "M";
//        case 2:
//            case 4 :
//        case 6 :
//        case 8 :
//            return "F";
//            default:
//            return -1;
//
//    }
//    if (firstDigit % 2 == 1) {
//        gender = "M"
//    } else {
//        gender = "F";
//    }
//    return gender;
}



    public static int retrieveBirthYear (String personalCode) { //49403136526
        //valideerimine
        if (personalCode == null) {
        return -1;

    } else if (personalCode.length() != 11){
            return -1;
        }

        int century = Integer.parseInt(personalCode.substring(0, 1));// nr 4
        int birthYear = Integer.parseInt(personalCode.substring(1, 3));// nr 94

        switch (century) {
            case 1:
            case 2:
                return 1800 + birthYear;//break pole vaja, kuna return nagunii jätab pooleli
            case 3 :
            case 4 :
                return 1900 + birthYear;
            case 5 :
            case 6 :
                return 2000 + birthYear;
            case 7 :
            case 8 :
                return 2100 + birthYear;
            default:
                return -1;

        }
    }

    private static boolean validatePersonalCode (String personalCode) {//49403136526
       if (personalCode == null) {
           return false;
       } else if (personalCode.length() != 11){
           return false;
        }
       String[] personalCodeParts = personalCode.split("");
       //String[11] {"4", "9", }
       int[] personalCodeDigits = new int[11];//teen uue int massiivi
        for (int i = 0; i < 11; i++){
            personalCodeDigits[i] = Integer.parseInt(personalCodeParts[i]);

            //splitib ära ja paneb int mapi, muudab kõik elemendid intiks
            //int[] digits = Stream.of(personalCode.split("")).mapToInt(Integer:: parseInt).toArray());
           // Integer[] digits = Stream.of(personalCode.split("")).map(s -> Integer.parseInt(s)).collect(Collectors.toList()).toArray(Integer[]) ::new);
        }
        int[] weights1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};
        int sum = 0;
        //summeerime kokku kaalu ja isikukoodi vastavate numbrite korrutised
        // sum = 1 * 4 +2 * 9 +...
        for (int i = 0; i < 10; i++) {
            sum = sum + weights1 [i] * personalCodeDigits[i];
        }
        int checkNumber = sum % 11;

        if (checkNumber != 10) {
            //tagastame true või false
           return personalCodeDigits[10] == checkNumber;
        } else {

        }

            int[] weights2 = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};
            sum = 0;
            //summeerime kokku kaalu ja isikukoodi vastavate numbrite korrutised
            // sum = 1 * 4 +2 * 9 +...
            for (int i = 0; i < 10; i++) {
                sum = sum + weights2[i] * personalCodeDigits[i];
            }
            checkNumber = sum % 11;

            if (checkNumber != 10) {
                //tagastame true või false
                return personalCodeDigits[10] == checkNumber;
            } else {
                return personalCodeDigits[10] == 0; //kui tulemus 10, siis kas viimane nr võrdub 0-ga
            }



    }

    //kui ei ole static, siis main-ist ei saa seda väljakutsuda
    //public void validateSomething ();
    //saab teha klassiga sama nimega objekti
    //ee.bcs.valiit.day6.MethodExcersises myMethodExcercisesObject = new ee.bcs.valiit.day6.MethodExcersises();
    //myMethodExcersisesObject.validateSomething();


}
