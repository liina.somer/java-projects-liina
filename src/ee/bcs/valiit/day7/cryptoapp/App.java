package ee.bcs.valiit.day7.cryptoapp;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class App {
//    private static List<String> alphabetLines;//App klassi kõik muutujad pääsevad ligi
//    private static Map<String, String> encryptionAlphabet;
//    private static Map<String, String> decryptionAlphabet;

    public static void main(String[] args) throws IOException {
        System.out.println("------------------------------");
        System.out.println("Vali IT spioonirakendus");
        System.out.println("------------------------------");

        Scanner scanner = new Scanner(System.in);
        Cryptor encryptor = new Encryptor("resources/alfabeet.txt");
        Cryptor decryptor = new Decryptor("resources/alfabeet.txt");

        while(true) {
            System.out.println("Mida soovid teha? [1- krüpteeri, 2- dekrüpteeri, 3- välju programmist]");
            String input = scanner.nextLine();
            switch (input){
                case "1":
                    System.out.println("1");
                    System.out.println("Sisesta tekst:");
                    String message = scanner.nextLine().toUpperCase();
                    System.out.println("Krüpteeritud tekst:");
                    System.out.println(encryptor.convert(message));
                    break;
                case "2":
                    System.out.println("2");
                    System.out.println("Sisesta krüpteeritud tekst:");
                    String encryptedMessage = scanner.nextLine().toUpperCase();
                    System.out.println("Dekrüpteeritud tekst:");
                    System.out.println(decryptor.convert(encryptedMessage));
                    break;
                case "3":
                    System.out.println("3");
                    System.out.println("Aitäh kasutamast!");
                    return;
                default:
                    System.out.println("Ei saanud aru");

            }
        }

//       String message = args[0];
//       Cryptor cryptor = new Encryptor("resources/alfabeet.txt");
//       String encryptedMessage = cryptor.convert(message);//saaks selle panna cryptor.convert() ja olemasolev sulgudesse, keerab teistpidi
//       System.out.println("Krüpteeritud sõnum: " + encryptedMessage);
//
//       Cryptor decryptor = new Decryptor("resources/alfabeet.txt");
//       String decryptedMessage = decryptor.convert(encryptedMessage);
//       System.out.println("Dekrüpteeritud sõnum: " + decryptedMessage);
    }
}

