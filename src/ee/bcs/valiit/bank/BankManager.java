package ee.bcs.valiit.bank;

import java.io.IOException;
import java.util.Scanner;

public class BankManager {
    public static void main(String[] args) throws IOException {
        //loeb enda sisse kõik kontod, mis on account tüüpi
        AccountService.loadAccounts("resources/kontod.txt");
        //kui eemaldada siis throws exception, siis küsib, kas panna try catch



        Scanner scanner = new Scanner(System.in);

        System.out.println("---------------------------------");
        System.out.println("VALI IT PANK");
        System.out.println("---------------------------------");

        System.out.println("Juhend:");
        System.out.println("EXIT - välju programmist");
        System.out.println("BALANCE<ACCOUNT> kuva kontoandmed");
        System.out.println("BALANCE<FIRST NAME> <LAST NAME> kuva kontoandmed");
        System.out.println("TRANSFER<FROM ACCOUNT> <TO ACCOUNT> <SUM > teosta ülekanne");

        while (true) {
            System.out.print("Sisesta käsklus: ");
            String input = scanner.nextLine();

            //Lõhume sisendi tükkideks
            String[] inputParts = input.split(" ");

            if ("EXIT".equalsIgnoreCase(inputParts[0])) {
                System.out.println("Nägemist!");
                return;
            } else if ("BALANCE".equalsIgnoreCase(inputParts[0])) {
                Account account = null;
                if (inputParts.length == 2) {//BALANCE<ACCOUNT NUMBER
                    account = AccountService.findAccount(inputParts[1]);
                } else if (inputParts.length == 3) {//// BALANCE <FIRST NAME> <LAST NAME>
                    account = AccountService.findAccount(inputParts[1], inputParts[2]);//viide AccountService klassi
                }
                displayAccountDetails(account);
            } else if ("TRANSFER".equalsIgnoreCase(inputParts[0])) {
                // TRANSFER <FROM ACCOUNT> <TO ACCOUNT> <SUM>
                if (inputParts.length < 4) {
                    System.out.println("Ebakorrektsed lähteandmed, ei saa ülekannet teha");
                } else {
                    double sum = getSumFromText(inputParts[3]);
                    TransferResult result = AccountService.transfer(inputParts[1], inputParts[2], sum);
                    displayTransferResult(result);
                }
            } else {
                System.out.println("Tundmatu käsklus!");
            }

            System.out.println("---------------------------------");

        }

    }


    private static double getSumFromText(String inputPart) {
        try {
            return Double.parseDouble(inputPart);
        } catch (NumberFormatException e) {
            return 0;
        }

    }

    private static void displayTransferResult(TransferResult result) {
        if (result.isSuccess()) {
            System.out.println("------------------------------------");
            System.out.println("ÜLEKANNE ÕNNESTUS");
            System.out.println("MAKSJA:");
            displayAccountDetails(result.getFromAccount());
            System.out.println("SAAJA:");
            displayAccountDetails(result.getToAccount());
        } else {
            System.out.println("------------------------------------");
            System.out.println("ÜLEKANNE EBAÕNNESTUS");
            System.out.println("PÕHJUS: " + result.getMessage());
        }
    }

    private static void displayAccountDetails(Account account) {
        if (account != null) {
            System.out.println("---------------------------------");
            System.out.println("KONTO ANDMED");
            System.out.printf("NIMI:\t\t%s %s\n", account.getFirstName(), account.getLastName());
            System.out.printf("KONTO:\t\t%s\n", account.getAccountNumber());
            System.out.printf("JÄÄK:\t\t%.2f EUR\n", account.getBalance());//%.2f- kaks komakohta

        } else {
            System.out.println("---------------------------------");
            System.out.println("Kontot ei leitud!");

        }
    }
}


