package ee.bcs.valiit.day7;

public class EnglishDog extends Dog {
    public EnglishDog(String name) {
        super(name);
    }

    @Override
    public void bark() {
        System.out.println("woof-woof");
    }
    //woof-woof; ruff-ruff; arf-arf; bow-wow; yap-yap (small dogs); yip-yip (very small dogs
}
