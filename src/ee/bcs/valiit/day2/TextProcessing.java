package ee.bcs.valiit.day2;

public class TextProcessing {
    public static void main(String[] args) {
        //Stringide defineerimine
        String text1 = "Tere!";
        String text2 = ("tere");
        //Stringide kokkuliitmine
        String text3 = text1 + text2;

        System.out.println("Text3: " + text3);

        String text4 = text1.concat("TERE!"); //concatenated? ehk siis ühendatud?
        System.out.println("Text4: " + text4);

        //antud juhul hakkab võrdlema sama mälupiirkonna aadressiga, text4 hakkab võrduma text3 mälupiirkonnaga
        //text4 objekt jääb peremeheta ja lõpuks garbage collector koristab selle millalgi ära
        //varasem text4 viide String text4 = text1.concat(text2); asendub ehk hakkab võrduma text3-ga
        //text4 = text3;
        //text4 = "abc";
        //System.out.println(text3);

        //nüüd annab printimisel true, sest võrdleb samu mälupiirkonna numbreid
        //nii ei saa stringe tegelikult võrrelda, kuna nii me võrdleme mälupiirkonna aadresse
        System.out.println(text3 == text4);

        System.out.println(".equals() " + text3.equals(text4));
        System.out.println(".equalsIgnoreCase() " + text3.equalsIgnoreCase(text4));

        //erinevaid variante on, kuidas saab neid printida t- jätab vahe, n-uus rida Escape sequences tabel materjalides
        String text5 = "Isa ütles:\n \"Tule siia!\"";
        System.out.println(text5);
        //varjestab jutumärgid
        String text6 = "Minu salajane fail: C:\\secret\\document.txt";
        System.out.println(text6);

    }
}
