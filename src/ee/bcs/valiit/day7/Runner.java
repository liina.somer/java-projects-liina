package ee.bcs.valiit.day7;

public class Runner extends Athlete {


    public Runner(String firstName, String lastName, int age, String gender, double height, double weight) {
        super(firstName, lastName, age, gender, height, weight);
    }

    @Override
    public void perform() {
        System.out.println("Running like crazy");
    }
}
