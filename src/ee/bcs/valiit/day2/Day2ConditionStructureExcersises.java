package ee.bcs.valiit.day2;

public class Day2ConditionStructureExcersises {
    public static void main(String[] args) {
        /*Ülesanne 2: valikute konstrueerimine
Defineeri String-tüüpi muutuja ja anna talle väärtuseks "Berlin".
Konstrueeri if-lause, mis kontrolliks, kas antud muutuja väärtus on "Milano".
Kui on, siis kirjuta standardväljundisse lause: "Ilm on soe." Kui mitte, siis kirjuta: "Ilm polegi kõige tähtsam!"*/
        System.out.println("Ex2");
        //args [] võtab tulemuse praegu käsurealt, aga args [1] puudub
        String city = args [0];
        //if (city == "Milano") võib praegu töötada, aga kui city väärtus tuleks näiteks käsurealt või kuskilt süsteemist või failist
        //siis see ei toimiks
         if (city.equals("Milano")) {
             System.out.println("Ilm on soe.");
         } else
             System.out.println("Ilm polegi kõige tähtsam!");

         /*Ülesanne 3: if, else if, else
Defineeri muutuja, mis hoiaks täisarvulist väärtust vahemikus 1 - 5. Väärtusta see muutuja.
Kasuta "if"-"else if"-"else" konstruktsiooni, et printida ekraanile kas "nõrk", "mitterahuldav",
"rahuldav", "hea", "väga hea" - vastavalt sellele, mis hinne eelpooldefineeritud muutujasse salvestatud.
          */
        System.out.println("Ex 3");
        int grade = Integer.parseInt (args[1]); //tuleb nii, kui tahta võtta käsurealt- Edit Configurations- Program arguments
        //int grade = 5;
        if (grade==1) {
            System.out.println("nõrk");

        } else if (grade==2){
            System.out.println("mitterahuldav");
        } else if (grade==3) {
            System.out.println("rahuldav");
        } else if (grade==4) {
            System.out.println("hea");
        } else if (grade == 5) {
            System.out.println("väga hea");
        } else {
            System.out.println("tundmatu väärtus");
        }

/*Ülesanne 4: switch
Defineeri muutuja, mis hoiaks täisarvulist väärtust vahemikus 1 - 5. Väärtusta see muutuja.
Kasuta switch-konstruktsiooni, et printida ekraanile kas "nõrk", "mitterahuldav", "rahuldav",
"hea", "väga hea" - vastavalt sellele, mis hinne eelpooldefineeritud muutujasse salvestatud.
 */
        System.out.println("Ex4");
        switch (grade) {
            case 1:
                System.out.println("nõrk");
                break;
            case 2:
                System.out.println("mitterahuldav");
                break;
            case 3:
                System.out.println("rahuldav");
                break;
            case 4:
                System.out.println("hea");
                break;
            case 5:
                System.out.println("väga hea");
                break;
            default:
                System.out.println("tundmatu väärtus");
                break;
        }
        /*Ülesanne 5: inline-if
Defineeri muutuja, mis hoiaks endas täisarvulist väärtust.
Kasutades inline-if konstruktsiooni, kirjuta standardväljundisse kas "Noor" või "Vana", vastavalt sellele,
kas muutuja väärtus on suurem, kui 100 või mitte.*/

        System.out.println("Ex 5");
        int myNumber = 101;
        String isOld = myNumber > 100 ? "Vana" : "Noor";
        System.out.println(isOld);


/*Ülesanne 6: inline-if
Defineeri muutuja, mis hoiaks endas täisarvulist väärtust.
Kasutades inline-if konstruktsiooni, kirjuta standardväljundisse kas "Noor", "Vana" või "Peaaegu vana", vastavalt sellele,
kas inimene vanus on alla saja, üle saja või täpselt sada.  : myNumber > 100 ? */
        //if (age < 100) {
        // isYoung = "Noor";
        // } else if (age == 100) {
           // isYoung = "Peaaegu vana";
        // } else

        //saaks kasutada uuesti String isOld (muuta väärtust), aga siis ei saa String ette panna,
        //lihtsalt isOld =
        System.out.println("Ex 6");
        String isYoung = (myNumber == 100 ? "Peaaegu vana" : (myNumber < 100 ? "Noor" : "Vana"));
        System.out.println(isYoung);


    }
}
