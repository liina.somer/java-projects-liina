package ee.bcs.valiit.testEx;

public class Rectangle {

    public static void main(String[] args) {
        System.out.println("The area of the rectangle is " + getArea(5, 6));

    }

    private static int getArea (int height, int width){
     return height * width;

    }
}
